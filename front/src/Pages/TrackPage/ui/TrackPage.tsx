import { MusicItem, useMusicService } from '../../../Entities/music';
import { useCallback, useEffect, useState } from 'react';
import { useTrackAction, useTrackState } from '../../../Entities/player/model/PlayerContext';
import { MusicInterface } from '../../../Shared/types/Music';
import { serverHost } from '../../../Shared/consts/server';
import { useOnPlay } from '../../../Entities/player/model/useOnPlay';
import { useParams } from 'react-router-dom';


export const TrackPage = () => {
  const { id } = useParams();
  
  const { getMusicByid, allMusics } = useMusicService();
  const { activeTrackId, pause } = useTrackState();
  
  const { pauseTrack, setActiveTrack } = useTrackAction();
  const [music, setMusic] = useState<MusicInterface>();

  const play = useOnPlay(allMusics?.map(music => music.id));

  const getMusic = useCallback(async () => {
    if(id) {
      const data = await getMusicByid(id);
      setMusic(data);
    }
  }, [id, getMusicByid]);

  useEffect(() => {
    getMusic();
  }, [getMusic]);
  
  return (
    <>
      {music && (
        <div>
          <img src={serverHost+music?.poster} alt="poster" />
          <div>{music?.text}</div>
          <div>{allMusics?.map(music => (
            <MusicItem 
              key={music.id} music={music}  
              setPlayer={true}
              playerState={pause}
              activeTrackId={activeTrackId} 
              playTrack={play} 
              pauseTrack={pauseTrack}
              setActiveTrack={setActiveTrack} 
            />
          ))}</div>
        </div>
      )}
    </>
  );
};
