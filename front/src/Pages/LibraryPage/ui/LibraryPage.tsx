import { useTrackAction, useTrackState } from '../../../Entities/player/model/PlayerContext';
import { LikeBtn } from '../../../Features';
import { MusicItem } from '../../../Entities/music';
import { useCheckAuthState } from '../../../Entities/session/indext';
import { useOnPlay } from '../../../Entities/player/model/useOnPlay';



export const LibraryPage = () => {
  const { authData } = useCheckAuthState();

  const { pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, pause } = useTrackState();

  const play = useOnPlay(authData?.user.favorites?.map(music => music.id));

  return (
    <div>
      <h2>Вам понравилось</h2>
      {authData?.user.favorites.map(music => (
        <MusicItem 
          music={music} 
          key={music.id} 
          setPlayer 
          LikeBtn={<LikeBtn music={music} />} 
          authData={authData} 
          playTrack={play} 
          playerState={pause} 
          activeTrackId={activeTrackId}
          setActiveTrack={setActiveTrack}
          pauseTrack={pauseTrack}
        />
      ))}
    </div>
  );
};
