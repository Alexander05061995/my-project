import { AuthForm } from '../../../Features';
import styles from './AuthPage.module.scss';

export const AuthPage = () => {
  return (
    <div className={styles.wrapper}>
      <AuthForm />
    </div>
  );
};

