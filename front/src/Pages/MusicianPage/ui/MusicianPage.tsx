import { MusicItem, useMusicService } from '../../../Entities/music';
import { useTrackAction, useTrackState } from '../../../Entities/player/model/PlayerContext';
import { useCheckAuthState } from '../../../Entities/session/indext';
import { useMusicianService } from '../../../Entities/musician';
import { useOnPlay } from '../../../Entities/player/model/useOnPlay';
import { useParams } from 'react-router-dom';




export const MusicianPage = () => {
  const { id } = useParams();
  const { musicByMusician } = useMusicService(id);
  const { musicianById } = useMusicianService(id);
  
  const { authData } = useCheckAuthState();
  const { pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, pause } = useTrackState();

  const play = useOnPlay(musicByMusician?.map(music => music.id));
  
  return (
    <div>
      <h2>{musicianById?.name}</h2>
      {musicByMusician?.map(music => (
        <MusicItem 
          setPlayer={true} 
          key={music.id} 
          music={music} 
          playTrack={play} 
          playerState={pause} 
          activeTrackId={activeTrackId} 
          pauseTrack={pauseTrack} 
          authData={authData}
          setActiveTrack={setActiveTrack} 
        />
      ))}
    </div>
  );
};
