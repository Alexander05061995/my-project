import { useTrackAction, useTrackState } from '../../../Entities/player/model/PlayerContext';
import { MusicItem } from '../../../Entities/music';
import { useCheckAuthState } from '../../../Entities/session/indext';
import { useGenresService } from '../../../Entities/genre';
import { useOnPlay } from '../../../Entities/player/model/useOnPlay';
import { useParams } from 'react-router-dom';

export const GenrePage = () => {
  const { id } = useParams();
  const { genreById } = useGenresService(id);
  
  const { authData } = useCheckAuthState();
  const { pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, pause } = useTrackState();

  const play = useOnPlay(genreById?.musics?.map(music => music.id));
  
  return (
    <div>
      <h2>{genreById?.name}</h2>
      {genreById?.musics.map(music => (
        <MusicItem 
          setPlayer={true} 
          key={music.id} 
          music={music} 
          playTrack={play} 
          playerState={pause} 
          activeTrackId={activeTrackId} 
          pauseTrack={pauseTrack} 
          authData={authData}
          setActiveTrack={setActiveTrack} 
        />
      ))}
    </div>
  );
};

