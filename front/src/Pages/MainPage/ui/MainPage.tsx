import { MusicsList } from '../../../Widgets/MusicList'; 
import { PopularMusicsList } from '../../../Widgets/PopularMusicList';
import { useCheckAuthState } from '../../../Entities/session/indext';

export const MainPage = () => {

  const { authData } = useCheckAuthState();

  return (
    <>
      {authData?.user.name && (
        <h1 style={{marginBottom: '30px'}}>Добро пожаловать {authData?.user.name}!</h1>
      )}
      <MusicsList />
      <PopularMusicsList />
    </>
  );
};
