import { GenreList } from '../../../Widgets/GenreList';
import { MusicianList } from '../../../Widgets/MusicianList';
import { PopularMusicsList } from '../../../Widgets/PopularMusicList';

export const NavigatorPage = () => {
  return (
    <>
      <GenreList />
      <MusicianList />
      <PopularMusicsList info/>
    </>
  );
};
