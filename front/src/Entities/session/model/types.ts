import { MusicInterface } from '../../../Shared/types/Music';

export interface CheckAuthDataInterface {
  user: {
    id: string;
    name: string;
    email: string;
    hashedPassword: string;
    role: string;
    avatarPath: string;
    createdAt: string;
    updatedAt: string;
    history: MusicInterface[];
    favorites: MusicInterface[],
  },
  accessToken: string;
  refreshToken: string;
}