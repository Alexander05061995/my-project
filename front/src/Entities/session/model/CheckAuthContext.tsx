import { ReactNode, createContext, useContext, useMemo, useReducer } from 'react';
import { MusicInterface } from '../../../Shared/types/Music';

interface AuthDataInterface {
  user: {
    id: string;
    name: string;
    email: string;
    hashedPassword: string;
    role: string;
    avatarPath: string;
    createdAt: string;
    updatedAt: string;
    history: MusicInterface[];
    favorites:  MusicInterface[],
    favoritesId: string[],
  },
  accessToken: string;
  refreshToken: string;
}

interface IinitialStateInterface {
  loading: boolean;
  authData: AuthDataInterface | undefined;
  error: string;
}

const initialState: IinitialStateInterface = {
  loading: false,
  authData: {
    user: {
      id: '',
      name: '',
      email: '',
      hashedPassword: '',
      role: '',
      avatarPath: '',
      createdAt: '',
      updatedAt: '',
      history: [],
      favorites: [],
      favoritesId: [],
    },
    accessToken: '',
    refreshToken: '',
  },
  error: '',
};

enum ActionTypes {
  POSTAUTHDATASUCCESS = 'POSTAUTHDATASUCCESS',
  POSTAUTHDATAFETCHING = 'POSTAUTHDATAFETCHING',
  POSTAUTHDATAERROR = 'POSTAUTHDATAERROR',
}

interface IPostDataSuccess {
  type: ActionTypes.POSTAUTHDATASUCCESS,
  payload: {
    user: {
      id: string;
      name: string;
      email: string;
      hashedPassword: string;
      role: string;
      avatarPath: string;
      createdAt: string;
      updatedAt: string;
      history: MusicInterface[],
      favorites: MusicInterface[],
      favoritesId: string[],
    },
    accessToken: string;
    refreshToken: string;
  },
}

interface IPostDataFetching {
  type: ActionTypes.POSTAUTHDATAFETCHING,
}

interface IPostDataError {
  type: ActionTypes.POSTAUTHDATAERROR,
  payload: string;
}

export type ActionsType = IPostDataSuccess | IPostDataFetching | IPostDataError;


const reducer = (state: IinitialStateInterface, action: ActionsType) => {
  switch (action.type) {
  case ActionTypes.POSTAUTHDATAFETCHING: 
    return { ...state, loading: true};
  case ActionTypes.POSTAUTHDATASUCCESS: 
    return { ...state, loading: false, authData: action.payload};
  case ActionTypes.POSTAUTHDATAERROR:
    return { ...state, loading: false, error: action.payload };
  default: return state;
  }
};


type AllActions = {
  postDataAuthFetching: () => void;
  postDataAuthSuccess: (payload: AuthDataInterface | undefined) => void;
  postDataAuthError: (payload: string) => void;
}

type ContextValue = {
  state: IinitialStateInterface;
  actions: AllActions;
}

const CheckAuthContext = createContext<ContextValue>({ state: initialState, actions: {} as AllActions });

const postDataAuthFetching = () => ({type: ActionTypes.POSTAUTHDATAFETCHING}) as IPostDataFetching;
const postDataAuthSuccess = (payload: AuthDataInterface | undefined) => ({type: ActionTypes.POSTAUTHDATASUCCESS, payload}) as IPostDataSuccess;
const postDataAuthError = (payload: string) => ({type: ActionTypes.POSTAUTHDATAERROR, payload}) as IPostDataError;

const CheckAuthProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const actions = useMemo(() => ({
    postDataAuthFetching: () => dispatch(postDataAuthFetching()),
    postDataAuthSuccess: (payload: AuthDataInterface | undefined) => dispatch(postDataAuthSuccess(payload)),
    postDataAuthError: (payload: string) => dispatch(postDataAuthError(payload)),
  }), [dispatch]);

  const value = useMemo(() => ({
    state,
    actions,
  }), [actions, state]);

  return (
    <CheckAuthContext.Provider value={value}>
      {children}
    </CheckAuthContext.Provider>
  );
};

const useBodyContext = () => useContext(CheckAuthContext);
const useCheckAuthAction = () => useBodyContext().actions;
const useCheckAuthState = () => useBodyContext().state;

export {
  CheckAuthProvider,
  useCheckAuthAction,
  useCheckAuthState,
};