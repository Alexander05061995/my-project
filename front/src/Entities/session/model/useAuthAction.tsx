import { AuthService } from '../../../Shared/api/AuthService/AuthService';
import { useCallback } from 'react';
import { useCheckAuthAction  } from './CheckAuthContext';

export const useAuthAction = () => {
  const {postDataAuthError, postDataAuthFetching, postDataAuthSuccess} = useCheckAuthAction();

  const myAuth = useCallback(async () => {
    try {
      postDataAuthFetching();
      const data = await AuthService.getNewTOkens();
      postDataAuthSuccess(data);
    } catch (error:any) {
      postDataAuthError(error);
    }
  }, [postDataAuthSuccess, postDataAuthFetching, postDataAuthError]);

  return {
    myAuth,
  };
};


