import { MusicianService } from '../../../Shared/api/MusicianService/MusicianService';
import { useQuery } from 'react-query';

export const useMusicianService = (id?:string) => {
  const {data: allMusician, isLoading: allMusicianLoading, error: allMusicianError } = useQuery({
    queryKey: 'getAllMusician', 
    queryFn: async () => await MusicianService.getAllMusician(),
  });

  const {data: musicianById, isLoading: musicianByIdLoading, error: musicianByIdError } = useQuery({
    queryKey: ['getMusicianById', id], 
    queryFn: async () => await MusicianService.getMusicById(id ? id : ''),
  });

  return {
    allMusician,
    allMusicianLoading,
    allMusicianError,
    musicianById,
    musicianByIdLoading,
    musicianByIdError,
  };
};