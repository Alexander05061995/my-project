import { useTrackAction } from './PlayerContext';

export const useOnPlay = (ids: any[] | undefined) => {
  const { setActiveTracks, playTrack } = useTrackAction();
  if(ids) {
    const play = () => {
      setActiveTracks(ids);
      playTrack();
    };
    return play;
  }
};