import { createContext, useContext, useMemo, useReducer } from 'react';

interface InitialState {
  pause: boolean;
  activeTrackId: string;
  activeTrackIds: string[]
}

const initialState: InitialState = {
  pause: true,
  activeTrackId: '',
  activeTrackIds: [],
};

enum ActionTypes {
  PAUSETRACK = 'PAUSETRACK',
  PLAYTRACK = 'PLAYTRACK',
  SETACTIVETRACK = 'SETACTIVETRACK',
  SETACTIVETRACKS = 'SETACTIVETRACKS',  
}

interface ISetActiveTrack {
  type: ActionTypes.SETACTIVETRACK;
  payload: string;
}

interface ISetActiveTracks {
  type: ActionTypes.SETACTIVETRACKS;
  payload: string[];
}

interface IPauseTrack {
  type: ActionTypes.PAUSETRACK;
}

interface IPlayTrack {
  type: ActionTypes.PLAYTRACK;
}

type AllActionsType = ISetActiveTrack | ISetActiveTracks | IPauseTrack | IPlayTrack;

const reducer = (state:InitialState, action:AllActionsType) => {
  switch (action.type) {
  case ActionTypes.SETACTIVETRACK:
    return {...state, activeTrackId: action.payload};
  case ActionTypes.SETACTIVETRACKS:
    return {...state, activeTrackIds: action.payload}; 
  case ActionTypes.PAUSETRACK:
    return {...state, pause: true};
  case ActionTypes.PLAYTRACK:
    return {...state, pause:  false};
  default:
    return state;
  }
};

type AllActions = {
  setActiveTrack: (id:string) => void;
  setActiveTracks: (ids: string[]) => void;
  playTrack: () => void;
  pauseTrack: () => void;
}

type ContextValue = {
  state: InitialState;
  actions: AllActions;
}

const TrackContex = createContext<ContextValue>({state: initialState, actions: {} as AllActions});

const setActiveTrack = (payload: string) => ({type: ActionTypes.SETACTIVETRACK, payload}) as ISetActiveTrack;
const setActiveTracks = (payload: string[]) => ({type: ActionTypes.SETACTIVETRACKS, payload}) as ISetActiveTracks;
const playTrack = () => ({type: ActionTypes.PLAYTRACK}) as IPlayTrack;
const pauseTrack = () => ({type: ActionTypes.PAUSETRACK}) as IPauseTrack;

const TrackProvider = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const actions = useMemo(() => ({
    setActiveTrack: (payload: string) => dispatch(setActiveTrack(payload)),
    setActiveTracks: (payload: string[]) => dispatch(setActiveTracks(payload)),
    playTrack: () => dispatch(playTrack()),
    pauseTrack: () => dispatch(pauseTrack()),
  }), [dispatch]);

  const value = useMemo(() => ({
    state,
    actions,
  }), [actions, state]);

  return (
    <TrackContex.Provider value={value}>
      {children}
    </TrackContex.Provider>
  );
};

const useBodyContext = () => useContext(TrackContex);
const useTrackAction = () => useBodyContext().actions;
const useTrackState = () => useBodyContext().state; 

export {
  TrackProvider,
  useTrackAction,
  useTrackState,
};