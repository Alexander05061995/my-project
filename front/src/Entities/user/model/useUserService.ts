import { UserService } from '../../../Shared/api/UserService/UserService';
import { useAuthAction } from '../../session/indext';
import { useMutation } from 'react-query';

export const useUserService = () => {
  const { myAuth } = useAuthAction();
  const { mutate: toggleFavorite, isLoading:toggleFavoriteLoading, error: toggleFavoriteError } = useMutation({
    mutationKey: 'getGenres', 
    mutationFn: async (musicId:string) => await UserService.toggleLike(musicId),
    onSuccess: () => {
      myAuth();
    },
  });

  return {
    toggleFavorite,
    toggleFavoriteLoading,
    toggleFavoriteError,
  };
};