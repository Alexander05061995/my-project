import { GenreService } from '../../../Shared/api/GenreService/GenreService';
import { useQuery } from 'react-query';

export const useGenresService = (id?:string) => {
  const { data: allGenres, isLoading: allGenresLoading, error: allGenresError } = useQuery({
    queryKey: 'getGenres', 
    queryFn: async () => await GenreService.getAllGenres(),
  });

  const { data: genreById } = useQuery({
    queryKey: ['getGenreById', id], 
    queryFn: () => GenreService.getGenreById(id ? id : ''),
  });

  return {
    allGenres,
    allGenresLoading,
    allGenresError,
    genreById,
  };
};