import { ChangeEvent, useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import { MusicService } from '../../../Shared/api/MusicService/MusicService';
import { useDebounce } from '../../../Shared/libs/hooks/useDebounce';


export const useMusicService = (musicianId?:string) => {
  const [search, setSearch] = useState('');
  const [page, setPage] = useState(1);
  const debouncedSearch = useDebounce(search, 500);

  const {data: allMusics, isLoading: allMusicsLoading, error: allMusicsError, refetch: allMusicsRefetch } = useQuery({
    queryKey: 'getMusic', 
    queryFn:  () => MusicService.getAllMusic(debouncedSearch, page),
  });

  const {data: musicByMusician, isLoading: musicByMusicianLoading, error: amusicByMusicianError, refetch: musicByMusicianRefetch } = useQuery({
    queryKey: ['musicByMusician', musicianId], 
    queryFn: async () => await MusicService.getMusicByMusician(musicianId ? musicianId : ''),
  });

  const handleSearchMusic = (e: ChangeEvent<HTMLInputElement>) => { 
    setSearch(e.target.value);
    // setTimeout(() => {
    //   allMusicsRefetch();
    // }, 550);
  };

  const loadMoreMusic = () => {
    setPage(prev => prev++);
  };

  const {mutateAsync: getMusicByid, isLoading: musicByidLoading, error: musicByidError} = useMutation({
    mutationKey: 'getMusicById',
    mutationFn: async (id:string) => await MusicService.getMusicById(id),
  });

  const {data: popularMusics, isLoading: popularMusicsLoading, error: popularMusicsError} = useQuery({
    queryKey: 'getPopularMusics', 
    queryFn: async () => await MusicService.getPopularMusics(),
  });



  return {
    allMusics,
    allMusicsLoading,
    allMusicsError,
    getMusicByid,
    musicByidLoading,
    musicByidError,
    popularMusics,
    popularMusicsLoading,
    popularMusicsError,
    handleSearchMusic,
    loadMoreMusic,
    debouncedSearch,
    allMusicsRefetch,
    search,
    musicByMusician,
  };
};