import { PauseButton, PlayButton } from '../../../../Shared/ui';
import { CheckAuthDataInterface } from '../../../session/model/types';
import { MusicInterface } from '../../../../Shared/types/Music';
import { classNames } from '../../../../Shared/libs/classNames/classNames';
import { serverHost } from '../../../../Shared/consts/server';
import styles from './MusicItem.module.scss';

interface MusicItemProps {
  music?: MusicInterface;
  setActiveTrack?: (id:string) => void;
  activeTrackId?: string;
  playTrack?: () => void;
  pauseTrack?: () => void;
  playerState?: boolean;
  setPlayer: boolean;
  className?: string
  isDuration?: boolean;
  LikeBtn?: JSX.Element;
  authData?: CheckAuthDataInterface;
}

export const MusicItem = (props: MusicItemProps) => {  
  const { music, setActiveTrack, activeTrackId, pauseTrack, playTrack, playerState, setPlayer, className, LikeBtn, authData } = props;

  const launchTrack = () => {

    if(!playTrack || !pauseTrack || !music) return;
    
    switch (playerState) {
    case playerState && activeTrackId !== music.id:
      playTrack();
      setActiveTrack!(music.id);
      pauseTrack();
      break;
    case !playerState && activeTrackId !== music.id:
      setActiveTrack!(music.id);
      pauseTrack();
      break;
    case !playerState && activeTrackId === music.id:
      pauseTrack();
      break;
    case playerState && activeTrackId === music.id:
      playTrack();
      break;
    default:
      pauseTrack();
      break;
    }
  };
  
  return (
    <div className={classNames(styles.musicItem, {[styles.activeItem]: music?.id === activeTrackId }, [className])}>
      <div className={styles.posterContainer}>
        <img className={styles.poster} src={serverHost+music?.poster} alt='poster' />
        <div className={classNames(styles.btnControlContainer, {[styles.activeBtnControlContainer]: music?.id === activeTrackId }, [])}>
          {setPlayer && (
            <div className={styles.activeBtn}>
              {playerState && music?.id === activeTrackId ? 
                <PauseButton onClick={launchTrack}/> : 
                <PlayButton  onClick={launchTrack}/>}
            </div>
          )}
        </div>
      </div>
      <div className={styles.info}>
        <h3 className={styles.musicName}>{music?.title}</h3>
        {music?.musician?.map(musician => (
          <h4 className={styles.musicianName} key={musician.id}>{musician.name}</h4>
        ))}
      </div>
      {authData && (
        <div className={styles.likeBtn}>
          {LikeBtn}
        </div>
      )}
    </div>
  );
};

