import './styles/index.scss';
import 'react-toastify/dist/ReactToastify.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Route, Routes } from 'react-router-dom';
import { AdminPage } from '../Pages/AdminPage';
import { AudioPlayer } from '../Widgets/AudioPlayer';
import { AuthPage } from '../Pages/AuthPage';
import { CheckAuthProvider } from '../Entities/session/indext';
import { GenrePage } from '../Pages/GenrePage';
import { Header } from '../Widgets/Header';
import { HystoryPage } from '../Pages/HistoryPage';
import { LibraryPage } from '../Pages/LibraryPage'; 
import { MainPage } from '../Pages/MainPage'; 
import { MusicianPage } from '../Pages/MusicianPage';
import { NavigatorPage } from '../Pages/NavigatorPage'; 
import { ProfilePage } from '../Pages/ProfilePage'; 
import { Sidebar } from '../Widgets/SideBar';
import { ToastContainer } from 'react-toastify';
import { TrackPage } from '../Pages/TrackPage';
import { TrackProvider } from '../Entities/player/model/PlayerContext';


function App() {
  const queryClient = new QueryClient();
  
  return (
    <QueryClientProvider  client={queryClient}>
      <CheckAuthProvider>
        <TrackProvider>
          <ToastContainer position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="dark"
            progressStyle={{backgroundColor: 'white'}}
            toastStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.1)' }}
          />
          <div className='container'>
            <Sidebar/>
            <main className={'main'}>
              <Header />
              <div className='layout'>
                <Routes>
                  <Route path='/' element={<MainPage />} />
                  <Route path='/admin' element={<AdminPage />} />
                  <Route path='/auth' element={<AuthPage />} />
                  <Route path='/library' element={<LibraryPage />} />
                  <Route path='/navigator' element={<NavigatorPage />} />
                  <Route path='/history' element={<HystoryPage />} />
                  <Route path='/profile' element={<ProfilePage />} />
                  <Route path='/music/:id' element={<TrackPage />} />
                  <Route path='/genres/:id' element={<GenrePage />} />
                  <Route path='/musicians/:id' element={<MusicianPage />} />
                </Routes>
              </div>
            </main>
            <AudioPlayer />
          </div>
        </TrackProvider>
      </CheckAuthProvider>
    </QueryClientProvider>
  );
}

export default App;
