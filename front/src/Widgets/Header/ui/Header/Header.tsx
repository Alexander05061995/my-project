import { Applink, Avatar, Icon } from '../../../../Shared/ui';
import { useAuthAction, useCheckAuthState } from '../../../../Entities/session/indext';
import { useContext, useEffect, useState } from 'react';
import { AvatarDropdown } from '../../../../Features';
import Burger from '../Burger/Burger';
import {ReactComponent as Logo} from '../../../../Shared/assets/images/logo.svg';
import { SearchField } from '../../../../Features/SearchField/index';
import { ToggleContext } from '../../../../Shared/libs/toggler/toggler';
import { classNames } from '../../../../Shared/libs/classNames/classNames';
import { serverHost } from '../../../../Shared/consts/server';
import styles from './Header.module.scss';
import { useMusicService } from '../../../../Entities/music';

export const Header = () => {
  const [coordsAvatart, setCoordsAvatart] = useState<{x: string | number, y: string | number}>({x: 0, y: 0});
  const [dropdownIsOpenAvatar, setDropdownIsOpenAvatar] = useState(false);
  const {isOpendModal, setIsOpendModal} = useContext(ToggleContext);
  const {authData, error, loading} = useCheckAuthState();
  const { myAuth } = useAuthAction();
  const { debouncedSearch, allMusicsRefetch } = useMusicService();

  const getCoordsAvatarDropdown = (e:React.MouseEvent<HTMLImageElement>) => {
    setDropdownIsOpenAvatar(!dropdownIsOpenAvatar);
    const coordsTarget = {
      x: e.currentTarget.getBoundingClientRect().x - 181,
      y: window.scrollY + e.currentTarget.getBoundingClientRect().y + 50,
    };
    
    if(coordsTarget.x < 0) {
      coordsTarget.x = 0; 
    }
    setCoordsAvatart(coordsTarget);
  };

  const changeMenuState = () => {
    setIsOpendModal(!isOpendModal);    
  };

  useEffect(() => {
    myAuth();
  }, [myAuth]);

  // useEffect(() => {
  //   allMusicsRefetch();
  // }, [debouncedSearch, allMusicsRefetch]);

  return (
    <header className={classNames(styles.header, {}, [])}>
      <div className={styles.container}>
        <div className={styles.topMenu}>
          <Burger className={styles.burger} onClick={changeMenuState} />
          <div className={styles.logo}>
            <Icon className={styles.icon} Svg={Logo} />
            <h2 className={styles.logoName}>Music</h2>
          </div>
        </div>
        <nav className={styles.nav}>
          <SearchField />
          {loading ? (
            <div>Загрузка</div>
          ) : (
            <>
              {!authData?.user.name ? (
                <Applink to='auth' className={styles.enterBtn}>
                  <span>Войти</span>
                </Applink>
              ) : (
                <div>
                  <Avatar
                    onClick={(e:React.MouseEvent<HTMLImageElement>) => getCoordsAvatarDropdown(e)}
                    source={serverHost+'/'+authData?.user.avatarPath} 
                    alt='avatar'
                  />
                  {dropdownIsOpenAvatar && (
                    <AvatarDropdown 
                      dropdownIsOpen={dropdownIsOpenAvatar} 
                      setDropdownIsOpen={setDropdownIsOpenAvatar} 
                      logo={authData?.user.avatarPath} 
                      name={authData?.user.name} 
                      coords={coordsAvatart} 
                      className={styles.dropdown} />
                  )}
                </div>
              )}
            </>
          )}
        </nav>
      </div>
    </header>
  );
};
