import { AppButton, Icon } from '../../../../Shared/ui';
import {ReactComponent as BurgerIcon} from '../../../../Shared/assets/images/burger.svg';
import { classNames } from '../../../../Shared/libs/classNames/classNames';
import styles from './Burger.module.scss';


interface BurgerProps {
  onClick: () => void;
  className?: string;
}

const Burger = ({onClick, className}: BurgerProps) => {
  return (
    <div onClick={onClick}>
      <AppButton className={classNames(styles.burger, {}, [className])}>
        <Icon Svg={BurgerIcon} />
      </AppButton>
    </div>
  );
};

export default Burger;