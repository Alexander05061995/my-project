import { useEffect, useState } from 'react';
import { classNames } from '../../../../Shared/libs/classNames/classNames';
import styles from './VolumeLine.module.scss';


interface VolumeLineProps {
  audio: HTMLAudioElement;
  setMuteState: (bool: boolean) => void;
}

const VolumeLine = ({ audio, setMuteState }: VolumeLineProps) => {
  const [volume, setVolume] = useState(audio.volume*100);

  const onChange = (e: any) => {
    setVolume(e.target.value);  
    audio.volume = e.target.value / 100;
  };

  useEffect(() => {
    if(volume <= 1) {
      setMuteState(true);
      setVolume(audio.volume*100);
    } else {
      setMuteState(false);
      setVolume(audio.volume*100);  
    }
  }, [audio.volume, setMuteState, volume]);
  
  return (
    <div className={classNames(styles.wrapper, {}, [])}>
      <input onChange={onChange} min={0} max={100} value={volume} type="range" className={styles.range} />
    </div>
  );
};

export default VolumeLine;