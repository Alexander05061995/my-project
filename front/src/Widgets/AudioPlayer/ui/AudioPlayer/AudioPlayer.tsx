import { MusicItem, useMusicService } from '../../../../Entities/music';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTrackAction, useTrackState } from '../../../../Entities/player/model/PlayerContext';
import { MusicInterface } from '../../../../Shared/types/Music';
import { PauseButton } from '../../../../Shared/ui';
import { PlayButton } from '../../../../Shared/ui'; 
import { PlayerArrowLeft } from '../../../../Shared/ui'; 
import { PlayerArrowRight } from '../../../../Shared/ui'; 
import PlayerTimeline from '../PlayerTimeline/PlayerTimeline';
import { SkipBackwardButton } from '../../../../Shared/ui/SkipBackwardButton/SkipBackwardButton';
import { SkipFrowardButton } from '../../../../Shared/ui/SkipFrowardButton/SkipFrowardButton';
import { SoundOffButton } from '../../../../Shared/ui'; 
import { SoundOnButton } from '../../../../Shared/ui'; 
import Timer from '../Timer/Timer';
import VolumeLine from '../VolumeLine/VolumeLine';
import { serverHost } from '../../../../Shared/consts/server';
import styles from './AudioPlayer.module.scss';


export const AudioPlayer = () => {
  const { playTrack, pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, activeTrackIds, pause } = useTrackState();
  const { getMusicByid} = useMusicService();
  const audio:HTMLAudioElement = useMemo(() => new Audio(), []);
  const intercalRef = useRef(setInterval(() => {}, 0));
  const [currentTrack, setCurrentTrack] = useState<MusicInterface>();
  const [muteState, setMuteState] = useState(false);

  const fastForward = () => {
    if(audio.ended) {
      return;
    }
    audio.currentTime = audio.currentTime + 5;
  };

  const fastBackward = () => {
    if(audio.currentTime === 0) {
      return;
    }
    audio.currentTime = audio.currentTime - 5;
  };

  const onPressedDownSkipForward = () => {
    intercalRef.current = setInterval(fastForward, 300);
  };

  const onPressedUpSkipForward = () => {
    clearInterval(intercalRef.current);
  };

  const onPressedDownSkipBackward = () => {
    intercalRef.current = setInterval(fastBackward, 300);
  };

  const onPressedUpSkipBackward = () => {
    clearInterval(intercalRef.current);
  };

  const setPreviosTrack = () => {
    let currentIndexTrack = activeTrackIds.findIndex(item => item === activeTrackId);
    let previosIndexTrack = --currentIndexTrack;
    if(previosIndexTrack < 0) {
      previosIndexTrack = activeTrackIds.length - 1;
    }
    setActiveTrack(activeTrackIds[previosIndexTrack]);
  };

  const setNextSong = useCallback(() => {
    let currentIndexTrack = activeTrackIds.findIndex(item => item === activeTrackId);
    let nextIndexTrack = ++currentIndexTrack;
    if(nextIndexTrack > activeTrackIds.length - 1) {
      nextIndexTrack = 0;
    }
    setActiveTrack(activeTrackIds[nextIndexTrack]);
  }, [activeTrackIds, setActiveTrack, activeTrackId]);

  //управление плеером из вне
  const launchTrack = useCallback(async (id:string) => {
    const data = await getMusicByid(id);
    setCurrentTrack(data); 
    if(pause) {
      if(`${serverHost}${data.audio}` === audio.src) { 
        audio.play();  
      } else {
        audio.src = `${serverHost}${data.audio}`;
        audio.play();
      } 
    } 
    else {
      audio.pause();
    }
  }, [audio, getMusicByid, pause]);

  useEffect(() => {
    if(activeTrackId && activeTrackIds) {      
      launchTrack(activeTrackId); 
    } 

    audio.addEventListener('ended', setNextSong);

    return () => {
      audio.removeEventListener('ended', setNextSong);
    };

  }, [audio, activeTrackId, activeTrackIds, launchTrack, setNextSong, pause]);

  //локальное управление плеером
  const play = useCallback(() => {
    if(pause) {
      audio.play();
      playTrack();
    } else {
      audio.pause();
      pauseTrack();
    }
  }, [pause, audio, pauseTrack, playTrack]);

  const soundOn = useCallback((audio: HTMLAudioElement) => {
    audio.muted = false;
    audio.volume = 0.5;
    setMuteState(false);
  }, []);

  const soundOff = useCallback((audio: HTMLAudioElement) => {
    audio.muted = true;
    audio.volume = 0;
    setMuteState(true);
  }, []);

  if(!activeTrackId && !activeTrackIds) return <></>;



  return (
    <>
      {activeTrackId && (
        <div className={styles.warapper}>
          <PlayerTimeline audio={audio} />
          <div className={styles.player}>
            <div className={styles.playerState}>
              <div className={styles.control}>
                <SkipBackwardButton onPointerDown={onPressedDownSkipBackward} onPointerUp={onPressedUpSkipBackward}  />
                <PlayerArrowLeft onClick={setPreviosTrack} />
                {pause ? (
                  <PauseButton onClick={play} />
                ) : (
                  <PlayButton onClick={play} />
                )}
                <PlayerArrowRight onClick={setNextSong}/>
                <SkipFrowardButton onPointerDown={onPressedDownSkipForward} onPointerUp={onPressedUpSkipForward} />
              </div>
              <Timer audio={audio} />
            </div>
            <div>
              <MusicItem music={currentTrack} setPlayer={false} />
            </div>
            <div>
              <VolumeLine setMuteState={setMuteState} audio={audio} />
              {!muteState ? <SoundOnButton onClick={() => soundOff(audio)} /> :  <SoundOffButton onClick={() => soundOn(audio)} />}
            </div>
          </div>
        </div>
      )}
    </>
  );
};
