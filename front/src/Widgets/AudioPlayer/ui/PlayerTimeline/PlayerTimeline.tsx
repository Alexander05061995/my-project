import { useRef, useState } from 'react';
import styles from './playertimeline.module.scss';

interface PlayerTimelineProps {
  audio: HTMLAudioElement;
}

const PlayerTimeline = (props: PlayerTimelineProps) => {
  const { audio } = props;
  const [currentTime, setCurrentTime] = useState(0);
  const currerTimeRef = useRef(0);

  audio.ontimeupdate = () => {
    currerTimeRef.current = audio.currentTime/audio.duration*100;
    setCurrentTime(currerTimeRef.current);
  };
  
  const changeTrackerPosition = (e:any) => {
    const click = e.pageX/e.target.clientWidth*100;
    audio.currentTime = click / 100 * audio.duration;    
  };
  return (
    <div onClick={changeTrackerPosition} className={styles.wrapper}>
      <div data-time={audio.currentTime}  style={{transform: `scaleX(${currentTime}%)`, transformOrigin: 'left'}} className={styles.timeline}></div>
    </div>
  );
};

export default PlayerTimeline;