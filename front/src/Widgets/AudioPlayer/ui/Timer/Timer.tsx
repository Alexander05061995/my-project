import { useCallback, useEffect, useState } from 'react';

interface TimerProps {
  audio: HTMLAudioElement;
}

const Timer = ({ audio }: TimerProps) => {
  const [timeState, setTimeState] = useState(0);
  const onChange = useCallback(() => {
    setTimeState(audio.currentTime);
  }, [audio.currentTime]);

  useEffect(() => {
    const inteval = setInterval(() => {
      onChange();
    }, 1000);
    onChange();

    return () => clearInterval(inteval);
  }, [onChange, audio.currentTime]);
  return (
    <div>
      <span>
        {Math.floor(timeState / 60) +
										':' +
										('0' + Math.floor(timeState % 60)).slice(-2)}
      </span>
    </div>
  );
};

export default Timer;