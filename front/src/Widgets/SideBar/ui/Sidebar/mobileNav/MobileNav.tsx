import { AppButton, Applink, Icon } from '../../../../../Shared/ui';
import {ReactComponent as CloseIcon} from '../../../../../Shared/assets/images/close.svg';
import {ReactComponent as IconPlus} from '../../../../../Shared/assets/images/plus.svg';
import {ReactComponent as Logo} from '../../../../../Shared/assets/images/logo.svg';
import { classNames } from '../../../../../Shared/libs/classNames/classNames';
import { createPortal } from 'react-dom';
import styles from './MobileNav.module.scss';
import useSidebarRoutes from '../../../../../Shared/libs/hooks/useSidebarRoutes';



interface MobileNavProps {
  isOpendModal: boolean;
  changeMenuState: () => void;
  className?: string;
}

const MobileNav = (props: MobileNavProps) => {
  const {changeMenuState, className, isOpendModal} = props;
  const routes = useSidebarRoutes();
  
  return createPortal(
    <div className={classNames(styles.aside, {}, [className, isOpendModal ? styles.open : styles.close])}>
      <div className={styles.topMenu}>
        <div onClick={changeMenuState} >
          <Icon className={styles.burger} Svg={CloseIcon}/>
        </div>
        <div className={styles.logo}>
          <Icon Svg={Logo} />
          <h2>Music</h2>
        </div>
      </div>
      <nav className={styles.nav}>
        <ul>
          {routes.map(route => (
            <li key={route.label} className={
              [
                styles.item, route.active ? styles.active : '',
              ]
                .join(' ')}>
              <Applink to={route.href} className={styles.link}>
                <Icon Svg={route.icon} className={styles.icon}/>
                <span>{route.label}</span>
              </Applink>
            </li>
          ))}
        </ul>
      </nav>
      <div className={styles.separator}></div>
      <AppButton className={styles.btn}>
        <Icon Svg={IconPlus} className={styles.icon}/>
        <span>Новый</span>
      </AppButton>
    </div>,
    document.body);
};

export default MobileNav;