import { AppButton, Applink, Icon } from '../../../../Shared/ui';

import Burger from '../../../Header/ui/Burger/Burger';
import { ReactComponent as IconPlus } from '../../../../Shared/assets/images/plus.svg';
import { ReactComponent as Logo } from '../../../../Shared/assets/images/logo.svg';
import MobileNav from './mobileNav/MobileNav';
import { ToggleContext } from '../../../../Shared/libs/toggler/toggler';
import styles from './Sidebar.module.scss';
import {useContext} from 'react';
import useSidebarRoutes from '../../../../Shared/libs/hooks/useSidebarRoutes';

export const Sidebar = () => {
  const {isOpendModal, setIsOpendModal} = useContext(ToggleContext);

  const changeMenuState = () => {
    setIsOpendModal(!isOpendModal);
  };
  const routes = useSidebarRoutes();
  
  return (
    <>
      <aside className={isOpendModal ? styles.aside : styles.asideShrink}>
        <div className={styles.topMenu}>
          <Burger className={styles.burger} onClick={changeMenuState} />
          <div className={styles.logo}>
            <Icon Svg={Logo} />
            <h2>Music</h2>
          </div>
        </div>
        <nav className={styles.nav}>
          <ul>
            {routes.map(route => (
              <li key={route.label} className={
                [
                  isOpendModal ? styles.item : styles.shrinkedItem, route.active ? styles.active : '',
                ]
                  .join(' ')}>
                <Applink to={route.href} className={isOpendModal ? styles.link : styles.shrinkedLink}>
                  <Icon Svg={route.icon} className={isOpendModal ? styles.icon : styles.iconShrinked}/>
                  <span>{route.label}</span>
                </Applink>
              </li>
            ))}
          </ul>
        </nav>
      
        {isOpendModal && (
          <>
            <div className={styles.separator}></div>
            <AppButton className={styles.btn}>
              <Icon Svg={IconPlus} className={styles.icon}/>
              <span>Новый</span>
            </AppButton>
          </>
        )}
      </aside>
      <MobileNav isOpendModal={isOpendModal} 
        changeMenuState={changeMenuState}/>
    </>
  );
};