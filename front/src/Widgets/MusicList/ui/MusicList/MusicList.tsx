import { useTrackAction, useTrackState } from '../../../../Entities/player/model/PlayerContext';
import { LikeBtn } from '../../../../Features';
import { MusicItem } from '../../../../Entities/music';
import styles from './musicliast.module.scss';
import { useCheckAuthState } from '../../../../Entities/session/indext';
import { useMusicService } from '../../../../Entities/music';
import { useOnPlay } from '../../../../Entities/player/model/useOnPlay';




export const MusicsList = () => {
  const { allMusics } = useMusicService();
  const { authData } = useCheckAuthState();
  const { pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, pause } = useTrackState();

  const play = useOnPlay(allMusics?.map(music => music.id));

  return (
    <>
      <h2 className={styles.title}>Вся музыка</h2>
      <div>
        {allMusics?.map(music => (
          <div key={music.id} className={styles.link}>
            <MusicItem
              setPlayer={true}
              playerState={pause}
              activeTrackId={activeTrackId} 
              playTrack={play} 
              pauseTrack={pauseTrack}
              setActiveTrack={setActiveTrack} 
              key={music.id} 
              music={music} 
              LikeBtn={<LikeBtn music={music} />}
              authData={authData}
            />  
          </div>
        ))}
      </div>
    </>
  );
};