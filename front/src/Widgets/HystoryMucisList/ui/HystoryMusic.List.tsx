import { useTrackAction, useTrackState } from '../../../Entities/player/model/PlayerContext';
import { MusicItem } from '../../../Entities/music';
import styles from './hystorymusiclist.module.scss';
import { useCheckAuthState } from '../../../Entities/session/indext';
import { useOnPlay } from '../../../Entities/player/model/useOnPlay';


export const HystoryMusicsList = () => {
  const { authData } = useCheckAuthState();
  const { pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, pause } = useTrackState();
  const play = useOnPlay(authData?.user?.history.map(music => music.id));

  // useEffect(() => {
  //   if(authData?.user) {
  //     setActiveTracks(authData?.user?.history.map(music => music.id));
  //   }
  // }, [authData?.user?.history, setActiveTracks, authData]);

  // if(allMusicsLoading) {
  //   return (
  //     <div>Загрузка</div>
  //   );
  // }


  return (
    <>
      <h2 className={styles.title}>История</h2>
      <div>
        {authData?.user?.history.map(music => (
          <MusicItem
            setPlayer={true}
            playerState={pause}
            activeTrackId={activeTrackId} 
            playTrack={play} 
            pauseTrack={pauseTrack}
            setActiveTrack={setActiveTrack} 
            key={music.id} music={music} 
          />
        ))}
      </div>
    </>
  );
};