import { Applink } from '../../../../Shared/ui';
import styles from './musicianlist.module.scss';
import { useMusicianService } from '../../../../Entities/musician';


export const MusicianList = () => {
  const { allMusician } = useMusicianService();
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>Исполнители</h2>
      <ul className={styles.list}>
        {allMusician?.map(musician => (
          <li key={musician.id} className={styles.item}>
            <Applink className={styles.link} to={`/musicians/${musician.id}`}>
              {musician.name}
            </Applink>
          </li>
        ))}
      </ul>
    </div>
  );
};

