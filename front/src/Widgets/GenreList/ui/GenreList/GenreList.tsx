import { Applink } from '../../../../Shared/ui';
import styles from './genrelist.module.scss';
import { useGenresService } from '../../../../Entities/genre';


export const GenreList = () => {
  const { allGenres } = useGenresService();
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>Жанры</h2>
      <ul className={styles.list}>
        {allGenres?.map(genre => (
          <li key={genre.id} className={styles.item}>
            <Applink className={styles.link} to={`/genres/${genre.id}`}>
              {genre.name}
            </Applink>
          </li>
        ))}
      </ul>
    </div>
  );
};
