import { useTrackAction, useTrackState } from '../../../Entities/player/model/PlayerContext';
import { LikeBtn } from '../../../Features';
import { Link } from 'react-router-dom';
import { MusicItem } from '../../../Entities/music';
import styles from './popularmusiclist.module.scss';
import { useMusicService } from '../../../Entities/music';
import { useOnPlay } from '../../../Entities/player/model/useOnPlay';


interface PopularMusicsListProps {
  info?: boolean;
}

export const PopularMusicsList = ({ info = false }: PopularMusicsListProps) => {
  const { popularMusics } = useMusicService();
  const { pauseTrack, setActiveTrack } = useTrackAction();
  const { activeTrackId, pause } = useTrackState();
  
  const play = useOnPlay(popularMusics?.map(music => music.id));

  return (
    <>
      <h2 className={styles.title}>Популярная музыка</h2>
      <div>
        {popularMusics?.map(music => (
          <Link key={music.id} to={`/music/${music.id}`} className={styles.link}>
            <MusicItem
              setPlayer={true}
              playerState={pause}
              activeTrackId={activeTrackId} 
              playTrack={play} 
              pauseTrack={pauseTrack}
              setActiveTrack={setActiveTrack}
              key={music.id} music={music} 
              LikeBtn={<LikeBtn music={music} />}
            />
            {info && (
              <div>
                <h4>Прослушиваний:</h4>
                <div>{music.listenings}</div>
                <h4>Поравилось:</h4>
                <div>{music.userIds.length}</div>
              </div>
            )}
          </Link>
        ))}
      </div>
    </>
  );
};