import { GenreInterface } from '../../types/Genre';
import { axiosAuth } from '../api';

export const GenreService = {
  async getAllGenres() {
    const response:GenreInterface[] = await axiosAuth.get('/genres').then((data) => data.data);
    return response;
  },
  async getGenreById(id:string) {
    const response:GenreInterface = await axiosAuth.get(`/genres/${id}`).then((data) => data.data);
    return response;
  },
};