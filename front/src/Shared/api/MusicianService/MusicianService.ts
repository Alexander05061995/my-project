import { MusicianInterface } from '../../types/Musician';
import { axiosAuth } from '../api';

export const MusicianService = {
  async getAllMusician() {
    const response: MusicianInterface[] = await axiosAuth.get('/musicians').then(data => data.data);
    return response;
  },

  async getMusicById(id:string) {
    const response: MusicianInterface = await axiosAuth.get(`/musicians/${id}`).then(data => data.data);    
    return response;
  },

  // async getPopularMusics() {
  //   const response: MusicInterface = await axiosAuth.get('musics/popular/most').then(data => data.data);
  //   return response;
  // },
};