import { axiosAuth } from '../api';

export const UserService = {
  async toggleLike(musicId: string) {
    const response: string = await axiosAuth.patch(`/users/profile/favorites/${musicId}`).then(data => data.data);
    return response;
  },
};