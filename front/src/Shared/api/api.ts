import Cookies from 'js-cookie';
import axios from 'axios';
import { serverApi } from '../consts/server';

export const axiosAuth = axios.create({
  baseURL: serverApi,
});

axiosAuth.interceptors.request.use((config) => {
  const accessToken = Cookies.get('accessToken');
  if (config.headers && accessToken) {
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
});