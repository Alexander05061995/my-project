import { MusicInterface } from '../../types/Music';
import { axiosAuth } from '../api';

export const MusicService = {
  async getAllMusic(search?: string, page?:number) {
    
    const response: MusicInterface[] = await axiosAuth.get('/musics', {params: {search, page}}).then(data => data.data);
    return response;
  },

  async getMusicById(id:string) {
    const response: MusicInterface = await axiosAuth.get(`/musics/all/ids/${id}`).then(data => data.data);    
    return response;
  },

  async getPopularMusics() {
    const response: MusicInterface[] = await axiosAuth.get('musics/popular/most').then(data => data.data);
    return response;
  },

  async getMusicByMusician(musicianId: string) {
    const response: MusicInterface[] = await axiosAuth.get(`musics/${musicianId}`).then(data => data.data);
    return response;
  },
};