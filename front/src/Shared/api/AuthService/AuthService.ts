import { removeTokensStorage, saveToStorage } from './Auth.helpers';
import { AuthResponseInterface} from '../../types/Auth';
import Cookies from 'js-cookie';
import { axiosAuth } from '../api';



export const AuthService =  {

  async registration(email: string, name: string, password:string) {
    const response:AuthResponseInterface = await axiosAuth.post('/auth/registration', {email, name, password}).then((data) => data.data);
    if(response) {
      saveToStorage(response);
    }
    return response;
  },

  async logIn(email: string, password: string) {
    const response:AuthResponseInterface = await axiosAuth.post('/auth/login', {email, password}).then((data) => data.data);
    if(response) {
      saveToStorage(response);
    }
    return response;
  },

  async logOut() {
    removeTokensStorage();
    localStorage.removeItem('user');
  },

  async getNewTOkens() {
    const refreshToken = Cookies.get('refreshToken');
    if(!refreshToken) {
      return;
    }
    const response:AuthResponseInterface  = await axiosAuth.post('auth/login/tokens', { refreshToken }).then((data) => data.data);
    if (response) {
      saveToStorage(response);
    } 
    return response;
  },
};