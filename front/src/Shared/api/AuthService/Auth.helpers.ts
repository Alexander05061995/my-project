import { AuthResponseInterface, TokensInterface } from '../../types/Auth';
import Cookies from 'js-cookie';


export const saveTokensStorage = (data: TokensInterface) => {
  Cookies.set('accessToken', data.accessToken);
  Cookies.set('refreshToken', data.refreshToken);
};

export const saveToStorage = (data: AuthResponseInterface) => {
  saveTokensStorage(data);
  localStorage.setItem('user', JSON.stringify(data.user));
};

export const removeTokensStorage = () => {
  Cookies.remove('accessToken');
  Cookies.remove('refreshToken');
};