import { classNames } from '../../libs/classNames/classNames';
import { forwardRef } from 'react';
import styles from './Input.module.scss';


interface InputProps {
  disabled?: boolean;
  type?: 'text' | 'password' | 'submit' | 'number';
  placeholder?:string;
  className?: string;
  required?: boolean;
  changeEvent?: (e:React.ChangeEvent<HTMLInputElement>) => void;
  value?: string;
  onInput?: (e: React.KeyboardEvent<HTMLInputElement>) => void
}

export const Input = forwardRef<HTMLInputElement, InputProps>((props: InputProps, ref) => {
  const { placeholder, disabled, type = 'text', required, className, changeEvent, value, onInput, ...otherProps } = props;
  
  return (
    <input
      onInput={onInput} 
      value={value}
      onChange={changeEvent}
      ref={ref}
      autoComplete='true'
      className={classNames(styles.input, {}, [className])} 
      type={type} disabled={disabled} 
      placeholder={placeholder}
      required={required}
      {...otherProps}
    />
  );
});
