import { ReactComponent as ArrowRightIcon } from '../../assets/images/rightArrow.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './playerArrowRight.module.scss';

interface PlayerArrowRightProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
}

export const PlayerArrowRight= (props: PlayerArrowRightProps) => {
  const { className, onClick, width, height } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <ArrowRightIcon className={styles.svg} />
    </button>
  );
};

