import { ReactComponent as FastBackwardIcon } from '../../assets/images/fastbackward.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './SkipBackwardButton.module.scss';

interface SkipBackwardButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
  onPointerDown?: () => void;
  onPointerUp?: () => void;
}

export const SkipBackwardButton= (props: SkipBackwardButtonProps) => {
  const { className, onClick, width, height, onPointerDown, onPointerUp } = props;
  return (
    <button 
      style={{width: `${width}`, height: `${height}`}} 
      onClick={onClick} 
      className={classNames(styles.button, {}, [className])}
      onPointerDown={onPointerDown}
      onPointerUp={onPointerUp}
    >
      <FastBackwardIcon className={styles.svg} />
    </button>
  );
};
