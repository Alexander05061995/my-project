import { ReactComponent as LikeIcon } from '../../assets/images/like.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './likebutton.module.scss';

interface LikeButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
}

export const LikeButton = (props: LikeButtonProps) => {
  const { className, onClick, width, height } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <LikeIcon className={styles.svg} />
    </button>
  );
};