import { classNames } from '../../libs/classNames/classNames';
import styles from './AppButton.module.scss';

export enum AppButtonTheme {
  PRIMARY = 'primary',
}

interface AppButtonProps extends React.ComponentPropsWithoutRef<'button'> {
  type?: 'button' | 'submit' | 'reset' | undefined;
  className?: string;
  theme?: AppButtonTheme,
  disabled?: boolean,
  onClick?: () => void;
}

export const AppButton = (props: AppButtonProps) => {
  const { children, className, type, theme = AppButtonTheme.PRIMARY, disabled, onClick } = props;
  return (
    <button onClick={onClick} disabled={disabled}  type={type} className={classNames(styles.btn, {[styles[theme]]: true}, [className] )}>
      {children}
    </button>
  );
};
