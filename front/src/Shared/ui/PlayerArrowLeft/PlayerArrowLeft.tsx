import { ReactComponent as ArrowLeftIcon } from '../../assets/images/lefftArrow.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './playerArrowLeft.module.scss';

interface PlayerArrowLeftProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
}

export const PlayerArrowLeft= (props: PlayerArrowLeftProps) => {
  const { className, onClick, width, height } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <ArrowLeftIcon className={styles.svg} />
    </button>
  );
};