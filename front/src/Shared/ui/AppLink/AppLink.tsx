import { Link, LinkProps } from 'react-router-dom';
import { classNames } from '../../libs/classNames/classNames';
import { memo } from 'react';
import styles from './AppLink.module.scss';

export enum AppLinkTheme {
  PRIMARY = 'primary',
}

interface AppLinkProps extends LinkProps {
  children?: React.ReactNode;
  className?: string;
  theme?: AppLinkTheme;
}

export const Applink = memo((props:AppLinkProps) => {
  const {to, children, className, theme = AppLinkTheme.PRIMARY, ...otherProps} = props;
  return (
    <Link to={to} className={classNames(styles.AppLink, { [styles[theme]]: true}, [className])} {...otherProps}>
      {children}
    </Link>
  );
});
