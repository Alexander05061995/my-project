import { ReactComponent as SoundOffIcon } from '../../assets/images/soundOff.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './soundofbutton.module.scss';

interface SoundOffButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
  onPointerDown?: () => void;
  onPointerUp?: () => void;
}

export const SoundOffButton= (props: SoundOffButtonProps) => {
  const { className, onClick, width, height, onPointerDown, onPointerUp } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <SoundOffIcon onPointerDown={onPointerDown} onPointerUp={onPointerUp} className={styles.svg} />
    </button>
  );
};