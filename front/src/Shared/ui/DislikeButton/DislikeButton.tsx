import { ReactComponent as DisLikeIcon } from '../../assets/images/disLike.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './dislikebutton.module.scss';

interface DisLikeButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
}

export const DisLikeButton = (props: DisLikeButtonProps) => {
  const { className, onClick, width, height } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <DisLikeIcon className={styles.svg} />
    </button>
  );
};