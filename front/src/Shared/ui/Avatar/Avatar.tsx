import { classNames } from '../../libs/classNames/classNames';
import styles from './Avatar.module.scss';

interface AvatarProps {
  className?: string;
  source: string;
  alt: string;
  width?: number;
  height?: number;
  onClick?: (e:React.MouseEvent<HTMLImageElement>) => void;
}

export const Avatar = (props: AvatarProps) => {
  const { className, source, alt, height = 45, width = 45, onClick } = props;
  return (
    <img onClick={onClick} height={height} width={width}  src={source} alt={alt} className={classNames(styles.avatar, {}, [className])} />
  );
};

export default Avatar;