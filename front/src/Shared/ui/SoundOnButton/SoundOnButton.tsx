import { ReactComponent as SoundOnIcon } from '../../assets/images/soundOn.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './soundonbutton.module.scss';

interface SoundOnButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
  onPointerDown?: () => void;
  onPointerUp?: () => void;
}

export const SoundOnButton= (props: SoundOnButtonProps) => {
  const { className, onClick, width, height, onPointerDown, onPointerUp } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <SoundOnIcon onPointerDown={onPointerDown} onPointerUp={onPointerUp} className={styles.svg} />
    </button>
  );
};