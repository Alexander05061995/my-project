import { ReactComponent as FastForwardIcon } from '../../assets/images/fastforward.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './SkipFrowardButton.module.scss';

interface SkipFrowardButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
  onPointerDown?: () => void;
  onPointerUp?: () => void;
}

export const SkipFrowardButton= (props: SkipFrowardButtonProps) => {
  const { className, onClick, width, height, onPointerDown, onPointerUp } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <FastForwardIcon onPointerDown={onPointerDown} onPointerUp={onPointerUp} className={styles.svg} />
    </button>
  );
};

