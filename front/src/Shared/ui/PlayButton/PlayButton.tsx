import { ReactComponent as PlayIcon } from '../../assets/images/play.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './playButton.module.scss';

interface PlayButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
}

export const PlayButton = (props: PlayButtonProps) => {
  const { className, onClick, width, height } = props;
  return (
    <button  style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <PlayIcon className={styles.svg} />
    </button>
  );
};
