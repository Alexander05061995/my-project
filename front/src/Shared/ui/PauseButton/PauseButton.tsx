import { ReactComponent as PauseIcon } from '../../assets/images/pause.svg';
import { classNames } from '../../libs/classNames/classNames';
import styles from './pauseButton.module.scss';

interface PauseButtonProps {
  className?:string;
  onClick?: () => void;
  width?: string;
  height?: string;
}

export const PauseButton = (props: PauseButtonProps) => {
  const { className, onClick, width, height } = props;
  return (
    <button style={{width: `${width}`, height: `${height}`}} onClick={onClick} className={classNames(styles.button, {}, [className])}>
      <PauseIcon className={styles.svg} />
    </button>
  );
};
