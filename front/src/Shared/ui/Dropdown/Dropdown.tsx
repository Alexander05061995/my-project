import React from 'react';
import { classNames } from '../../libs/classNames/classNames';
import { createPortal } from 'react-dom';
import styles from './Dropdown.module.scss';


interface DropdownProps {
  children: React.ReactNode;
  className?: string;
  coords?: {x: string | number, y: string | number};
  setDropdownIsOpen?: (bool: boolean) => void;
}

export const Dropdown = (props: DropdownProps) => {
  const { children, className, coords, setDropdownIsOpen } = props;
  return createPortal(
    <div className={styles.wrapper} onClick={ setDropdownIsOpen ? () => setDropdownIsOpen!(false) : () => {}}>
      <div style={{ top: coords?.y, left: coords?.x }} className={classNames(styles.dropdown, {}, [className])}>
        {children}
      </div>
    </div>, document.body);
};
