import { classNames } from '../../libs/classNames/classNames';
import { memo } from 'react';
import styles from './Icon.module.scss';



interface IconProps {
    className?: string;
    Svg: any
}

export const Icon = memo((props: IconProps) => {
  const { className, Svg} = props;

  return (
    <Svg className={classNames(styles.icon, {}, [className])} />
  );
});