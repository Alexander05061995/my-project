import { MusicInterface } from './Music';

export interface GenreInterface {
  id: string,
  name: string,
  musics: MusicInterface[]
}