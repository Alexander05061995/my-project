import { MusicInterface } from './Music';

export interface MusicianInterface  {
  id: string;
  name: string;
  photo: string;
  musics: MusicInterface[]
}