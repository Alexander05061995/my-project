export interface MusicInterface {
  id: string,
  userIds: string[];
  poster: string;
  title: string;
  text: string;
  rating: string | number;
  audio: string;
  listenings: string,
  musicianIds: string[];
  genresIds: string[];
  genres: [
      {
          id: string;
          name: string;
          musicsIds: string[];
      }
  ],
  musician: [
      {
          id: string;
          name: string;
          photo: string;
          musicsIds: string[];
      }
  ]

}