import { MusicInterface } from './Music';

export interface TokensInterface {
  accessToken: string;
  refreshToken: string;
}

export interface AuthResponseInterface {
  user: {
    id: string;
    name: string;
    email: string;
    hashedPassword: string;
    role: string;
    avatarPath: string;
    createdAt: string;
    updatedAt: string;
    history: MusicInterface[];
    favorites: MusicInterface[];
    favoritesId: string[],
  },
  accessToken: string;
  refreshToken: string;
}