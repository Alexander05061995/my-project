import { ReactNode, useState } from 'react';
import { createContext } from 'react';



interface TogglerInterface {
  isOpendModal: boolean;
  setIsOpendModal: (bool: boolean) => void;
}

export const ToggleContext = createContext<TogglerInterface>({isOpendModal: false, setIsOpendModal: () => {}});

export const TogglerContextFunc = ({children}: {children: ReactNode}) => {
  const [isOpendModal, setIsOpendModal] = useState(false);
  return (
    <ToggleContext.Provider value={{isOpendModal, setIsOpendModal}}>
      {children}
    </ToggleContext.Provider>
  );
};