import {ReactComponent as IconHome} from '../../assets/images/home.svg'; 
import {ReactComponent as IconLibrary} from '../../assets/images/library.svg' ;
import {ReactComponent as IconNavigator} from '../../assets/images/navigator.svg' ;
import { useLocation } from 'react-router-dom';
import { useMemo } from 'react';


const useSidebarRoutes = () => {
  const {pathname} = useLocation();

  const routes = useMemo(() => [
    { 
      label: 'Главная', 
      href: '/', 
      icon: IconHome, 
      active: pathname === '/',
    },
    { 
      label: 'Навигатор', 
      href: '/navigator', 
      icon: IconLibrary, 
      active: pathname === '/navigator',
    },
    {
      label: 'Библиотека', 
      href: '/library',
      icon: IconNavigator,
      active: pathname === '/library',
    },
  ], [pathname]);

  return routes;
};

export default useSidebarRoutes;