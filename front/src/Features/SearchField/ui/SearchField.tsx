import { Dropdown, Input } from '../../../Shared/ui';
import { MusicItem, useMusicService } from '../../../Entities/music';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import styles from './searchdropmodal.module.scss';



export const SearchField = () => {
  const [coordsDropDown, setCoordsDropDown] = useState<{x: string | number, y: string | number}>({x: 0, y: 0});
  const [dropdownIsOpenSearch, setDropdownIsOpenSearch] = useState(false);
  const { handleSearchMusic, debouncedSearch, allMusics, allMusicsRefetch, search, allMusicsLoading } = useMusicService();

  const getCoordsSearchDropdown = (e:React.MouseEvent<HTMLImageElement | HTMLInputElement>) => {
    setDropdownIsOpenSearch(true);
    const coordsTarget = {
      x: e.currentTarget.getBoundingClientRect().x,
      y: window.scrollY + e.currentTarget.getBoundingClientRect().y + 50,
    };
    
    if(coordsTarget.x < 0) {
      coordsTarget.x = 0; 
    }
    setCoordsDropDown(coordsTarget);
  };

  useEffect(() => {
    allMusicsRefetch();
  }, [debouncedSearch, allMusicsRefetch]);

  return (
    <div>
      <Input className={styles.input} changeEvent={handleSearchMusic} onInput={(e:any) => getCoordsSearchDropdown(e)} value={search} placeholder='Поиск трека' />
      {debouncedSearch && dropdownIsOpenSearch &&(
        <div className={styles.dropdownWrapper}>
          <Dropdown className={styles.dropdown} coords={coordsDropDown} setDropdownIsOpen={setDropdownIsOpenSearch}>
            {allMusics?.map(music => (
              <div key={music.id}>
                {music.title.toLowerCase().includes(debouncedSearch.toLowerCase()) && (
                  <Link to={`/music/${music.id}`}>
                    <MusicItem  setPlayer={false} music={music} key={music.id}  />
                  </Link>
                )}
              </div>
            ))}
            {allMusics?.length === 0 && (
              <div>ничего не найдено</div>
            )}
          </Dropdown>
        </div>
      )}
    </div>
  );
};

