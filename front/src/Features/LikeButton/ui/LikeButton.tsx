import { DisLikeButton } from '../../../Shared/ui/DislikeButton/DislikeButton';
import { LikeButton } from '../../../Shared/ui/LikeButton/LikeButton';
import { MusicInterface } from '../../../Shared/types/Music';
import { useCheckAuthState } from '../../../Entities/session/indext';
import { useUserService } from '../../../Entities/user';

interface LikeBtnProps {
  music: MusicInterface
}

export const LikeBtn = ({music}: LikeBtnProps) => {
  const { toggleFavorite } = useUserService();
  const { authData } = useCheckAuthState();
  return (
    <div>
      {authData?.user.favoritesId.includes(music?.id) ? 
        <DisLikeButton onClick={() => toggleFavorite(music?.id)} /> : 
        <LikeButton onClick={() => toggleFavorite(music?.id)} />}
    </div>
  );
};
