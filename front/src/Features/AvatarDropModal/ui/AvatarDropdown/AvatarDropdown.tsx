import { Applink, Avatar, Dropdown, Icon } from '../../../../Shared/ui';
import { classNames } from '../../../../Shared/libs/classNames/classNames';
import { serverHost } from '../../../../Shared/consts/server';
import styles from './AvatarDropdown.module.scss';
import { useRoutesAvatar } from '../../model/routes';

interface AvatarDropdownProps {
  className?: string;
  coords?: {x: string | number, y: string | number};
  name?: string;
  logo?: string;
  setDropdownIsOpen?: (bool: boolean) => void;
  dropdownIsOpen?: boolean
} 

export const AvatarDropdown = (props: AvatarDropdownProps) => {
  const { className, coords, name, logo, setDropdownIsOpen, dropdownIsOpen } = props;
  const routes = useRoutesAvatar();

  const closeDropdown = () => {
    if(setDropdownIsOpen) {
      setDropdownIsOpen(!dropdownIsOpen);
    }
  };

  return (
    <div onClick={closeDropdown}>
      <Dropdown setDropdownIsOpen={setDropdownIsOpen} coords={coords} className={classNames(styles.dropdown, {}, [className])}>
        <div className={styles.userInfoWrapper}>
          <Avatar source={serverHost+'/'+logo} alt='avatar' />
          <h3 className={styles.name}>{name}</h3>
        </div>
        <ul>
          {routes.map(route => (
            <li key={route.href} className={styles.listItem}>
              <Applink to={route.href} className={styles.link}>
                <Icon Svg={route.icon} className={styles.icon} />
                <span onClick={() => route.onCLick()}>{route.label}</span>
              </Applink>
            </li>
          ))}
        </ul>
      </Dropdown>
    </div>
  );
};
