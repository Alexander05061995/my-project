import { AuthService } from '../../../Shared/api/AuthService/AuthService';
import { ErrorLogOut } from './ErrorType';
import { toast } from 'react-toastify';
import { useAuthAction } from '../../../Entities/session/indext';
import { useMutation } from 'react-query';

export const useLogOut = () => {
  const { myAuth } = useAuthAction();
  const { mutate: logOutFn } = useMutation(
    {
      mutationKey: 'logOut',
      mutationFn: async () => await AuthService.logOut(),
      onSuccess: () => {
        toast('Вы вышли из системы!');
        myAuth();
      },
      onError: (error:ErrorLogOut ) => {
        toast(error.response.data.message);
      },
    },
  );

  return {
    logOutFn,
  };
};