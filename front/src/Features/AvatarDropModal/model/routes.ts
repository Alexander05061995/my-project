import {ReactComponent as AdminIcon} from '../../../Shared/assets/images/admin.svg';
import {ReactComponent as AvatarExitIcon} from '../../../Shared/assets/images/exit.svg';
import {ReactComponent as AvatarHistoryIcon} from '../../../Shared/assets/images/historyIcon.svg';
import {ReactComponent as AvatarHomeIcon} from '../../../Shared/assets/images/avatarHome.svg';
import { useLogOut } from './useLogOut';
import { useMemo } from 'react';

export const useRoutesAvatar = () => {

  const { logOutFn } = useLogOut();

  const routes = useMemo(() => [
    { 
      label: 'Профиль', 
      href: '/profile', 
      icon: AvatarHomeIcon,
      onCLick: () => {},
    },
    { 
      label: 'История', 
      href: '/history', 
      icon: AvatarHistoryIcon, 
      onCLick: () => {},
    },
    {
      label: 'Выйти', 
      href: '/',
      icon: AvatarExitIcon,
      onCLick: logOutFn,
    },
    {
      label: 'Админ-панель',
      href: '/admin',
      icon: AdminIcon,
      onCLick: () => {},
    },
  ], [logOutFn]);

  return routes;
};
