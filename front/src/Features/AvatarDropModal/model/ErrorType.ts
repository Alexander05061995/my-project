export interface ErrorLogOut {
  response: {
    data: {
      message: string
    }
  }
}