import { AuthService } from '../../../Shared/api/AuthService/AuthService';
import { ErrorType } from './types/ErrorType';
import { toast } from 'react-toastify';
import { useMutation } from 'react-query';





interface AuthDataInput {
  email: string;
  name?: string;
  password: string;
}

export const useAuth = () => {
  const {mutate: logIn, data: dataLogin, error: loginError, isSuccess: logInSuccess} = useMutation({
    mutationKey: 'register', 
    mutationFn: async (variables:AuthDataInput) => await AuthService.logIn(variables.email, variables.password), 
    onSuccess: () => {
      toast('Успешно вошли в систему!');
    },
    onError: (error: ErrorType) => {
      toast(error.response.data.message);
    },
  });

  const { mutate: registerFn, data: dataRegister, error: registerError, isSuccess: registerFnSuccess} = useMutation({
    mutationKey: 'register', 
    mutationFn: async (variables:AuthDataInput) => await AuthService.registration(variables.email, variables.name ? variables.name : '', variables.password), 
    onSuccess: () => {
      toast('Зарегистрированы!');
    },
    onError: (error: ErrorType) => {
      toast(error.response.data.message);
    },
  });

  return {
    registerFn,
    logIn,
    registerError,
    dataLogin,
    loginError,
    logInSuccess,
    registerFnSuccess,
    dataRegister,
  };

};

