import * as yup from 'yup';

export const schema = (formName: 'Регистрация' | 'Войти') => yup.object().shape({
  name: yup.string().when('name', {
    is: (val:string) => val === undefined && formName === 'Регистрация' || val && val.length < 5 && formName === 'Регистрация' || val === '' && formName === 'Регистрация',
    then: (schema) => schema.required('Поле обязательно для заполнения').min(5, 'Имя должно содержать минимум 5 символов'),
    otherwise: (schema) => schema.min(0).notRequired(),
  }),
  email: yup.string().email('Некорректный email').required('Поле обязательно для заполнения'),
  password: yup.string().min(4, 'Пароль должен содержать минимум 4 символа').max(10, 'Максимальная длина пароля 10 символов').required('Поле обязательно для заполнения'),
}, [
  ['name', 'name'],
]).required();