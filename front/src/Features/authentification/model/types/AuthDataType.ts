export interface AuthDataType {
  email: string;
  name?: string;
  password: string;
}