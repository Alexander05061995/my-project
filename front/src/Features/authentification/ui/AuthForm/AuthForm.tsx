import { AppButton, Input } from '../../../../Shared/ui';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useEffect, useState } from 'react';
import { IFormInput } from '../../model/types/FormInputType';
import { schema } from '../../model/Schema';
import styles from './AuthForm.module.scss';
import { useAuth } from '../../model/useAuth';
import { useAuthAction } from '../../../../Entities/session/indext';
import { useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

export const AuthForm = () => {
  const [nameForm, setNameForm] = useState<'Регистрация' | 'Войти'>('Войти');
  const [isLoading, setIsloading] = useState(false);
  const navigate = useNavigate();
  const { logIn, registerFn, logInSuccess, registerFnSuccess} = useAuth();
  const { myAuth } = useAuthAction();

  const { handleSubmit, register, formState: { errors }, reset } = useForm<IFormInput>({
    defaultValues: {
      email: '',
      name: '',
      password: '',
    },
    mode: 'all',
    resolver: yupResolver<IFormInput>(schema(nameForm)),
  });

  useEffect(() => {
    if(logInSuccess) {
      reset();
      navigate('/');
      myAuth();
    } 
    if(registerFnSuccess) {
      reset();
      navigate('/');
      myAuth();
    } 
  }, [logIn, registerFn, registerFnSuccess, logInSuccess, navigate, reset, myAuth]);

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    if(nameForm === 'Войти' ) {
      setIsloading(true);
      logIn(data);
      setIsloading(false);
    } 
    
    if(nameForm === 'Регистрация') {
      setIsloading(true);
      registerFn(data);
      setIsloading(false);
    }
  };

  return (
    <>
      <h1 className={styles.title}>{nameForm}</h1>
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        {nameForm === 'Регистрация' && (
          <div className={styles.inputContainer}>
            <label className={styles.label}>Имя</label>
            <Input 
              disabled={isLoading} 
              className={styles.input} 
              placeholder='Имя' {...register('name')} />
            {errors.name  && (
              <div style={{color: 'red'}}>
                {errors.name.message}
              </div>
            )}
          </div>
        )}
        <div className={styles.inputContainer}>
          <label className={styles.label}>Адресс почты</label>
          <Input 
            disabled={isLoading} 
            className={styles.input} 
            placeholder='Адресс почты' 
            {...register('email')}
          />
          {errors.email  && (
            <div style={{color: 'red'}}>
              {errors.email.message}
            </div>
          )}
        </div>
        <div className={styles.inputContainer}>
          <label className={styles.label}>Пароль</label>
          <Input
            changeEvent={() => {}}
            required
            disabled={isLoading} 
            className={styles.input} 
            placeholder='Пароль' 
            type='password' 
            {...register('password')}/>
          {errors.password  && (
            <div style={{color: 'red'}}>
              {errors.password.message}
            </div>
          )}
        </div>
        <AppButton type='submit' disabled={isLoading} className={styles.btn}>
          {nameForm}
        </AppButton>
        <div>
          {nameForm === 'Войти' && (
            <span>Нет аккаунта? <span className={styles.linkRegister} onClick={() => setNameForm('Регистрация')}>Зарегистрироваться</span> </span>
          )}
          {nameForm === 'Регистрация' && (
            <span>Есть аккаунт? <span className={styles.linkRegister} onClick={() => setNameForm('Войти')}>Войти</span> </span>
          )}
        </div>
      </form>
    </>
  
  );
};
