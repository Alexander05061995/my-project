import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'prisma.service';
import { CreateGenreDto } from './dto/create-genre.dto';

@Injectable()
export class GenreService {
  constructor(private readonly prismaService: PrismaService) {}

  async getById(id: string) {
    const genre = await this.prismaService.genreModel.findUnique({
      where: {
        id
      },
      include: {
        musics: true
      }
    })

    if(!genre) {
      throw new NotFoundException('Жанр не найден!')
    }

    return  genre;
  }

  async getAll() {
    const genres = await this.prismaService.genreModel.findMany({
      include: {
        musics:true
      }
    });

    if(!genres) {
      throw new NotFoundException('Жанры не найден!')
    }

    return genres
  }

  async update(id: string, dto: CreateGenreDto) {
    const updatedGenre = await this.prismaService.genreModel.update({
      where: {
        id
      },
      data: {
        name: dto.name
      }
    })

    if(!updatedGenre) {
      throw new NotFoundException('Жанр не найден!')
    }

    return updatedGenre
  }

  async create(dto: CreateGenreDto) {
    const exsistsGenre = await this.prismaService.genreModel.findUnique({
      where: {
        name: dto.name
      }
    })

    if(exsistsGenre) {
      throw new BadRequestException('Такой жанр уже существует!')
    }
    const cretedGenre = await this.prismaService.genreModel.create({
      data: {
        name: dto.name
      }
    })

    if(!cretedGenre) {
      throw new NotFoundException('Жанр не создан!')
    }

    return cretedGenre
  }

  async delete(id: string) {

    const musics = await this.prismaService.music.findMany({
      where: {
        genresIds: {
          has: id
        }
      }
    })

    for(const music of musics) {
      const updatedMusicIds = music.genresIds.filter(ident => ident !==id);

      await this.prismaService.music.update({
        where: {
          id: music.id
        },
        data: {
          genresIds: updatedMusicIds
        }
      })
  
    }

    const deletedGenre = await this.prismaService.genreModel.delete({
      where: {
        id
      }
    })

    if(!deletedGenre) {
      throw new NotFoundException('Такого жанране существует!')
    }

    return deletedGenre
  }

}
