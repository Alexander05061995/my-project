import { Controller, Get, Post, Body, Delete, Param, Put, ValidationPipe, UsePipes, HttpCode, Patch } from '@nestjs/common';
import { GenreService } from './genre.service';
import { CreateGenreDto } from './dto/create-genre.dto';
import { Auth } from 'src/auth/decorators/auth.decorator';

@Controller('genres')
export class GenreController {
  constructor(private readonly genreService: GenreService) {}

  @Get()
  async getAll() {
    return this.genreService.getAll()
  }

  @Get(':id')
  async geyById(@Param('id') id: string) {
    return this.genreService.getById(id)
  }

  @UsePipes(new ValidationPipe())
  @Post('create')
  // @Auth('ADMIN')
  async create(@Body() dto: CreateGenreDto ) {
    return this.genreService.create(dto)
  }

  @Delete(':id')
  @HttpCode(200)
  @Auth('ADMIN')
  async delete(@Param('id') id: string) {
    return this.genreService.delete(id)
  }

  @UsePipes(new ValidationPipe())
  @Put(':id')
  @Auth('ADMIN')
  async update(@Param('id') id: string, @Body() dto: CreateGenreDto) {
    return this.genreService.update(id, dto)
  }



}
