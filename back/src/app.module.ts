import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { PrismaService } from 'prisma.service';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { FileModule } from './file/file.module';
import { MusicModule } from './music/music.module';
import { GenreModule } from './genre/genre.module';
import { MusicianModule } from './musician/musician.module';

@Module({
  imports: [AuthModule, ConfigModule.forRoot(), UserModule, FileModule, MusicModule, GenreModule, MusicianModule],
  controllers: [AppController],
  providers: [AppService, PrismaService],
})
export class AppModule {}
