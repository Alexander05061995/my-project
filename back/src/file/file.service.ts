import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import * as fs from 'fs';
import { path } from 'app-root-path';
import { ensureDirSync, writeFileSync, } from 'fs-extra';
import * as uuid from 'uuid';

export enum FileType {
  AUDIO = 'audio',
  IMAGE = 'image',
  AVATAR = 'avatar'
}

@Injectable()
export class FileService {
  createFile(type: FileType, file): string {
    try {
      const uploadFolder = `${path}/uploads/${type}`;
      ensureDirSync(uploadFolder);

      const fileExtension = file.originalname.split('.').pop();

      const fileName = uuid.v4() + '.' + fileExtension;

      writeFileSync(`${uploadFolder}/${fileName}`, file.buffer);

      return '/uploads/' + type + '/' + fileName;
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  

  removeFile(file): string {
  
    try {
      if(file.audio) {
        fs.unlink(`${path}/${file.audio}`, (err => {
          if (err) console.log(err);
          else {
            console.log("Музыка удалена!");
          }
        }));
      }

      if(file.poster) {
          fs.unlink(`${path}/${file.poster}`, (err => {
            if (err) console.log(err);
            else {
              console.log("Изображение удалено!");
            }
          }));
      }

      if(file.photo) {
        fs.unlink(`${path}/${file.photo}`, (err => {
          if (err) console.log(err);
          else {
            console.log("Изображение удалено!");
          }
        }));
    }

      return 'Файл удален!'
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

