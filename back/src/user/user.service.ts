import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'prisma.service';
import { UpdateUserDto } from './dto/update-user-dto';
import * as bcrypt from 'bcrypt'
import { FileService, FileType } from 'src/file/file.service';


@Injectable()
export class UserService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly fileService: FileService
    ) { }

  async getById(id: string) {
    const user = await this.prismaService.user.findUnique({
      where: {
        id
      },
    })

    if (!user) throw new NotFoundException('Пользователь не найден');

    return user
  }

  async updateProfile(id: string, dto: UpdateUserDto) {
    const user = await this.prismaService.user.findUnique({
      where: {
        id
      }
    })
    console.log(dto);
    
    const updatedUser = await this.prismaService.user.update({
      where: {
        id
      },
      data: {
        email: dto.email ? dto.email : user.email,
        hashedPassword:   dto.password ? bcrypt.hashSync(dto.password, 7) : user.hashedPassword,
        role: dto.role ? dto.role : user.role,
        avatarPath: dto.avatarPath ? dto.avatarPath : user.avatarPath
      }
    })

    return updatedUser;
  }

  async getCount() {
    const count =  await this.prismaService.user.count()
    return count;
  }

  async getAll(searchTerm?: string) {
    const users = await this.prismaService.user.findMany({
      where: {
        name: searchTerm
      }
    })

    return users
  }


  async deleteUser(id:string) {
    return await this.prismaService.user.delete({
      where: {
        id
      }
    })
  }

  async toggleFavorite(userId: string, musicId: string) {
    const user = await this.getById(userId);

    if(!user) {
      throw new NotFoundException('Пользователь не найден')
    }

    const isExist = user.favoritesId.some(id => id == musicId)
    
    await this.prismaService.user.update({
      where: {
        id: user.id
      },
      data: {
        favorites: {
          [isExist ? 'disconnect': 'connect']: {
            id: musicId
          }
        }
      }
    })

    return { message: 'Success' }

  }

  async getFavoriteMovies(id: string) {
    const favorites = await this.prismaService.user.findMany({
      where: {
        id
      },
      include: {
        favorites: true
      }
    })

    return favorites;
  }

  async saveHistory(userId: string, dto: UpdateUserDto) {
    const user = await this.prismaService.user.update({
      where: {
        id: userId,
      },
      data: {
        history: {
          connect: [{id: dto.musicId}]
        }
      },
      include: {
        history: true
      }
    })

    return user
  }
}
