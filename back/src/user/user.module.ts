import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { PrismaService } from 'prisma.service';
import { FileService } from 'src/file/file.service';

@Module({
  controllers: [UserController],
  providers: [UserService, PrismaService, FileService]
})
export class UserModule {}
