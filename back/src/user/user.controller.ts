import { Controller, Get, Query, Delete, Param, HttpCode, Post, Patch, Body, ValidationPipe, UsePipes } from '@nestjs/common';
import { UserService } from './user.service';
import { CurrentUser } from './decorators/user.decorator';
import { Auth } from 'src/auth/decorators/auth.decorator';
import { UpdateUserDto } from './dto/update-user-dto';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get(':id')
  // @Auth('ADMIN')
  async getById(@Param('id') id:string) {
    return this.userService.getById(id)
  }

  @UsePipes(new ValidationPipe)
  @Patch('profile')
  @Auth()
  async updateProfile(@CurrentUser('id') userId: string,@Body() dto: UpdateUserDto) {
    return this.userService.updateProfile(userId, dto)
  }

  @Get('count')
  @Auth('ADMIN')
  async getCount() {
    return this.userService.getCount();
  }

  @Get()
  // @Auth('ADMIN')
  async getAll(@Query('searchTerm') searchTerm?: string) {
    return this.userService.getAll(searchTerm)
  }

  @Delete(':id')
  @HttpCode(200)
  // @Auth('ADMIN')
  async deleteUser(@Param('id') id: string) {
    return this.userService.deleteUser(id)
  }

  @Patch('profile/favorites/:musicId')
  @HttpCode(200)
  @Auth()
  async toggleFavorite(@CurrentUser('id') userId: string, @Param('musicId') musicId: string) {
    return this.userService.toggleFavorite(userId, musicId)
  }

  @Get('profile/favorites')
  @Auth()
  async getFavorites(@CurrentUser('id') userId: string) {
    return this.userService.getFavoriteMovies(userId)
  }

  @Post('hystory/music')
  @HttpCode(200)
  @Auth('USER')
  async saveHistory(@CurrentUser('id') userId: string, @Body() dto: UpdateUserDto) {        
    return this.userService.saveHistory(userId, dto)
  }
}
