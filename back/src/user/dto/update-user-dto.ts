import { Role } from "@prisma/client";
import { IsEmail, IsString, MinLength, IsOptional } from "class-validator";

export class UpdateUserDto {

  @IsEmail()
  @IsOptional()
  email?: string;
  @IsString()
  @MinLength(6)
  @IsOptional()
  @IsString()
  password?: string;
  @IsOptional()
  role?: Role
  @IsOptional()
  @IsString()
  avatarPath?: string;
  @IsOptional()
  @IsString()
  musicId: string
}