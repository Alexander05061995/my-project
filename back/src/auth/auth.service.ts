import { BadRequestException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { PrismaService } from 'prisma.service';
import { AuthDto } from './dto/auth.dto';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';
import { RefreshTokenDto } from './dto/refresh-token.dto';
import * as bcrypt from 'bcrypt'

@Injectable()
export class AuthService {
  constructor(private readonly prismaService: PrismaService, private jwtService: JwtService) { }
  
  async registration(dto: AuthDto) {
    const existingUser = await this.prismaService.user.findUnique({
      where: {
        email: dto.email
      }
    })

    if(existingUser) {
      throw new BadRequestException('Такой пользователь уже существует!')
    }

    const password = await bcrypt.hash(dto.password, 7)

    const user = await this.prismaService.user.create({
      data: {
        email: dto.email,
        hashedPassword: password,
        name: dto.name,
        role: "USER",
      }
    })

    if (!user) {
      throw new NotFoundException('Пользователь не зарегистрирован!');
    }

    const tokens = await this.generateTokens(user.id)

    return {
      user: this.returnUserFields(user),
      ...tokens
    }
  }

  async login(dto: AuthDto) {
    const user = await this.prismaService.user.findUnique({
      where: {
        email: dto.email
      },
      include: {
        history: true
      }
    })

    if(!user) {
      throw new NotFoundException('Такой пользователь не зарегистрирован!');
    }

    const password = await bcrypt.compare(dto.password, user.hashedPassword);

    if(!password) {
      throw new UnauthorizedException('Пароль неверный')
    }

    const tokens = await this.generateTokens(user.id);

    return {
      user,
      ...tokens
    }
  }

  async getTokens(dto: RefreshTokenDto) {
    if(!dto.refreshToken) {
      throw new UnauthorizedException('Неверный токен!');
    }
    
    let token;
    try {
      token = await this.jwtService.verifyAsync(dto.refreshToken);
    } catch (error) {
      throw new UnauthorizedException('Неверный токен!');
    }

    const user = await this.prismaService.user.findUnique({
      where: {
        id: token.id
      },
      include: {
        history: {
          include: {
            musician: true,
          }
        },
        favorites: {
          include: {
            favoriteUsers: true
          }
        }
      }
    })

    if(!user) {
      throw new UnauthorizedException('Такой пользователь не зарегистрирован!');
    }

    const tokens = await this.generateTokens(user.id)

    return {
      user,
      ...tokens
    }
  }

  private async generateTokens(userId: string) {
    const data = { id: userId };
    const accessToken = await this.jwtService.signAsync(data, {
      expiresIn: '1h',
    });
    const refreshToken = await this.jwtService.signAsync(data, {
      expiresIn: '15d',
    });

    return {
      accessToken,
      refreshToken
    }
  }

  private returnUserFields(user: User) {
    return {
      id: user.id,
      email: user.email,
      name: user.name,
      img: user.avatarPath
    }
  }
}
