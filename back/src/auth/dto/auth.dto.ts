import { IsEmail, MinLength, MaxLength, IsString, IsOptional} from "class-validator";

export class AuthDto {

  @IsEmail()
  email: string;

  @MinLength(4)
  @MaxLength(20)
  @IsString()
  password: string;

  @IsOptional()
  @IsString()
  name: string;
}