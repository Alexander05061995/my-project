import { AuthGuard } from "@nestjs/passport";
import { UseGuards, applyDecorators } from '@nestjs/common';
import { TypeRole } from "../auth.interface";
import { JwtAuthGuard } from "../guards/jwt.guard";
import { OnlyAdminGuard } from "../guards/admin.guard";
import { Role } from "@prisma/client";

// export const Auth = () => UseGuards(AuthGuard('jwt'))


export function Auth(role: Role = 'USER') {
	return applyDecorators(
		role === 'ADMIN'
			? UseGuards(JwtAuthGuard, OnlyAdminGuard)
			: UseGuards(JwtAuthGuard)
	);
}