import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'prisma.service';
import { MusicianDto } from './dto/musicain-dto';
import { FileService, FileType } from 'src/file/file.service';

@Injectable()
export class MusicianService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly fileService: FileService
    ) {}

  async getAll(searchTerm?: string) {
    const musicians = await this.prismaService.musician.findMany({
      where: {
        name: searchTerm
      },
      include: {
        musics: true
      }
    })

    return musicians
  }

  async byId(id: string) {
    const musician = await this.prismaService.musician.findUnique({
      where: {
        id
      }
    })

    if(!musician) {
      throw new NotFoundException('Исполнитель не найден!')
    }

    return musician
  }


  async createMusician(dto: MusicianDto, picture: Express.Multer.File) {
    
    const photo = this.fileService.createFile(FileType.IMAGE, picture)
    const musician = await this.prismaService.musician.create({
      data: {
        name: dto.name,
        photo: photo
      }
    })

    if(!musician) throw new NotFoundException('Ошибка при создании исполнителя!');

    return musician
  }

  async update(id:string, dto: MusicianDto, picture?: Express.Multer.File) {

    const musicain = await this.byId(id);

    let newPhoto = '';
    if(picture) {
      newPhoto = this.fileService.createFile(FileType.IMAGE, picture);
      this.fileService.removeFile(musicain)
    } 

    if(!musicain) {
      throw new NotFoundException('Исполнитель не найден!')
    }

    const updatedMusician = await this.prismaService.musician.update({
      where: {
        id
      },
      data: {
        name: dto.name ? dto.name : musicain.name,
        photo: newPhoto ? newPhoto : musicain.photo
      }
    })

    return updatedMusician;
  }

  async delete(id: string) {
    const musicain = await this.prismaService.musician.delete({
      where: {
        id
      }
    })

    const musics = await this.prismaService.music.findMany({
      where: {
        musicianIds: {
          has: id
        }
      }
    })

    for (const music of musics) {
      const updatedMusicians = music.musicianIds.filter(indent => indent !== id);

      await this.prismaService.music.update({
        where: {
          id: music.id
        },
        data: {
          musicianIds: updatedMusicians
        }
      })
    }
    this.fileService.removeFile(musicain)

    return musicain
  }
}
