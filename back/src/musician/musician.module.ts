import { Module } from '@nestjs/common';
import { MusicianService } from './musician.service';
import { MusicianController } from './musician.controller';
import { PrismaService } from 'prisma.service';
import { FileService } from 'src/file/file.service';

@Module({
  controllers: [MusicianController],
  providers: [MusicianService, PrismaService, FileService]
})
export class MusicianModule {}
