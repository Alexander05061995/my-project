import { Controller,Get, Param, Query, UsePipes, Body, ValidationPipe, Delete, Post, UseInterceptors, UploadedFiles, Patch } from '@nestjs/common';
import { MusicianService } from './musician.service';
import { MusicianDto } from './dto/musicain-dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { Auth } from 'src/auth/decorators/auth.decorator';

@Controller('musicians')
export class MusicianController {
  constructor(private readonly musicianService: MusicianService) {}

  @Get() 
  async getAllMusicians(@Query('searchTerm') searchTerm?: string) {
    return this.musicianService.getAll(searchTerm)
  }

  @Get(':id')
  async getMusicianById(@Param('id') id:string) {
    return this.musicianService.byId(id)
  }

  @UsePipes(new ValidationPipe())
  @Post()
  // @Auth('ADMIN')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'picture', maxCount: 1 },
    { name: 'audio', maxCount: 1 },
  ]))
  async createMusician(@Body() dto: MusicianDto, @UploadedFiles() files: {picture:Express.Multer.File[], audio:Express.Multer.File[]}) {
    const {picture} = files;
    return this.musicianService.createMusician(dto, picture[0]);
  }

  @UsePipes(new ValidationPipe())
  @Patch(':id')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'picture', maxCount: 1 },
    { name: 'audio', maxCount: 1 },
  ]))
  async updateMusician(@Body() dto: MusicianDto, @Param('id') id: string,  @UploadedFiles() files?: {picture:Express.Multer.File[], audio:Express.Multer.File[]}) {
    const {picture} = files;
    return this.musicianService.update(id, dto, picture[0]);
  }

  @Delete(':id')
  async deleteMusician(@Param('id') id: string) {
    return this.musicianService.delete(id)
  }
}
