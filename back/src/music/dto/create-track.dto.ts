import { IsArray, IsInt, IsString } from "class-validator";

export class CreateTrackDto {

  // @IsString()
  // poster: string;

  @IsString()
  title: string;

  @IsString()
  text: string;

  @IsInt()
  listenings?: number;

  @IsInt()
  rating?: number;

  @IsArray()
  @IsString({each: true})
  genres?: string;

  @IsArray()
  @IsString({each: true})
  musician?: string;
}