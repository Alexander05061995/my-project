import { Body, Controller, Delete, Get, Post, Query, UploadedFiles, UseInterceptors, HttpCode, Param, Put } from '@nestjs/common';
import { MusicService } from './music.service';
import { CreateTrackDto } from './dto/create-track.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { Auth } from 'src/auth/decorators/auth.decorator';

@Controller('musics')
export class MusicController {
  constructor(private readonly musicService: MusicService) {}

  @Get() 
  async getAll(@Query() search: {search?: string, page?: number}) {
    return this.musicService.getAllMusic(search)
  }

  @Post('create')
  // @Auth('ADMIN') РАСКОМЕНТИРУЙ!!
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'picture', maxCount: 1 },
    { name: 'audio', maxCount: 1 },
  ]))
  async create(@Body() dto: CreateTrackDto, @UploadedFiles() files: {picture:Express.Multer.File[], audio:Express.Multer.File[]}) {
    
    const {picture, audio} = files;
    
    return this.musicService.createMusic(dto, picture[0], audio[0])
  }

  @Delete(':id')
  @HttpCode(200)
  // @Auth('ADMIN') РАСКОМЕНТИРУЙ!!
  async delete(@Param('id') id: string) {
    return this.musicService.deleteMusic(id)
  }

  @Get(':id')
  async getByMusician(@Param('id') id: string) {
    return this.musicService.getByMusician(id)
  }

  @Post(':id')
  async updateListenings(@Param('id') id: string) {
    return this.musicService.updateListenings(id)
  }

  @Get('popular/most')
  async getPopuldarMusics() {
    return this.musicService.getMostPopular()
  }

  @Get('all/ids/:id')
  // @Auth('ADMIN')
  async getMusicianById(@Param('id') id:string) {
    return this.musicService.getById(id)
  }
}
