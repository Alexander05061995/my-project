import { Module } from '@nestjs/common';
import { MusicService } from './music.service';
import { MusicController } from './music.controller';
import { FileService } from 'src/file/file.service';
import { PrismaService } from 'prisma.service';

@Module({
  controllers: [MusicController],
  providers: [MusicService, FileService, PrismaService]
})
export class MusicModule {}
