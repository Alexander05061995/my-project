import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { PrismaService } from 'prisma.service';
import { CreateTrackDto } from './dto/create-track.dto';
import { FileService, FileType } from 'src/file/file.service';

@Injectable()
export class MusicService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly fileService: FileService,
  ) { }

  async getByMusician(musicianId: string) {
    const files = await this.prismaService.music.findMany({
      where: {
        musician: {
          some: {
            id: musicianId
          }
        }
      }
    })

    if (files) {
      return files
    } else {
      throw new NotFoundException('Файлы не найдены!')
    }
  }

  async getAllMusic(search: {search?: string, page?: number} ) {
  
    const PER_PAGE = 10;
    const files = await this.prismaService.music.findMany({
      where: {
        title: { 
          contains: search?.search ? search?.search.slice(0,1).toUpperCase() + search?.search.slice(1).toLowerCase() : ''
        },
      },
      include: {
        genres: true,
        musician: true
      },
      skip: search.page ? (Number(search.page) - 1) * PER_PAGE : 0, 
      take: PER_PAGE,
      orderBy: {
        title: 'desc'
      }
    })

    return files;
  }

  async createMusic(dto: CreateTrackDto, picture: Express.Multer.File, audio: Express.Multer.File) {
    
    if(!dto.text || !dto.title || !dto.genres || !dto.musician) {
      throw new NotFoundException('Нет текста песни, названия, жанра или исполнителя')
    }
    const audioPath = this.fileService.createFile(FileType.AUDIO, audio);
    if(!audioPath) {
      throw new NotFoundException('Аудио файл не найден!')
    }
    const picturePath = this.fileService.createFile(FileType.IMAGE, picture);
    if(!picturePath) {
      throw new NotFoundException('Постер файл не найден!')
    }
    const track = await this.prismaService.music.create({
      data: {
        audio: audioPath,
        poster: picturePath,
        text: dto.text,
        title: dto.title,
        listenings: 0,
        rating: 0,
        genres: {
          connect: [{id: dto.genres}],
        },
        musician: {
          connect: [{id: dto.musician}]
        }
      }
    })

    if(!track) {
      throw new BadRequestException('Что-то пошло не так')
    }

    return track;
  }

  async deleteMusic(id: string) {
    console.log(id, 'delete');
    

    const tr = await this.prismaService.music.findUnique({
      where: {
        id
      },
    })

    const musicians = await this.prismaService.musician.findMany({
      where: {
        musicsIds: {
          has: id,
        },
      },
    });

    for (const musician of musicians) {
      const updatedMusicsIds = musician.musicsIds.filter(ident => ident !== id);
  
      await this.prismaService.musician.update({
        where: {
          id: musician.id,
        },
        data: {
          musicsIds: updatedMusicsIds,
        },
      });
    }

    const genres = await this.prismaService.genreModel.findMany({
      where: {
        musicsIds: {
          has: id,
        },
      },
    });

    for (const genre of genres) {
      const updatedMusicsIds = genre.musicsIds.filter(ident => ident !== id);
  
      await this.prismaService.genreModel.update({
        where: {
          id: genre.id,
        },
        data: {
          musicsIds: updatedMusicsIds,
        },
      });
    }

        await this.prismaService.music.delete({
      where: {
        id
      },
    })

    const file = this.fileService.removeFile(tr)

    return file
  }
  

  async updateListenings(id:string) {
    const updatedMusic = await this.prismaService.music.update({
      where: {
        id
      },
      data: {
        listenings: {
          increment: 1
        } 
      }
    })

    if(!updatedMusic) {
      throw new NotFoundException('Трек не найден!')
    }
    
    return updatedMusic;
  }

  async getMostPopular() {
    const result = await this.prismaService.music.findMany({
      where: {
        listenings: {
          gt: 10
        }
      }
    })

    return result
  }

  async getById(id: string) {
    const music = await this.prismaService.music.findUnique({
      where: {
        id
      },
      include: {
        musician: true,
      }
    })

    if(!music) {
      throw new NotFoundException('Трек не найден!')
    }

    return music
  }

}
